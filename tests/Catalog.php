<?php

namespace Tests\GiiftCore\UltraVoucher;

use Giift\Model\ICategory;
use Giift\Model\IDmoCategory;
use Giift\Model\ITag;
use Giift\Model\Service\IBuyable;
use Giift\Model\Service\IDmo;
use Giift\UltraVoucher\Api;
use PHPUnit\Framework\TestCase;
use Pimple\Container;
use Ramsey\Uuid\Uuid;

class Catalog extends TestCase
{
    /** @var \GiiftCore\UltraVoucher\Catalog */
    private $catalogSync = null;

    protected function setUp()
    {
        parent::setUp();
        $this->catalogSync = new \GiiftCore\UltraVoucher\Catalog;
    }

    protected function tearDown()
    {
        parent::tearDown();
        \Mockery::close();
    }

    private function brandsResponse(): array
    {
        //@codingStandardsIgnoreStart
        return [
            'http_code' => 200,
            'status' => 'success',
            'code' => 'SSR001',
            'message' => 'Success get Brand data',
            'data' => [
                [
                    'brand_code' => 'IDMR',
                    'name' => 'Indomaret',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Indomaret_Logo.png',
                    'description' => 'Voucher Indomaret Digital Code adalah Voucher Belanja non fisik yang terdiri dari 16 Digit Kode Voucher. Voucher Belanja Indomaret Digital ini dapat digunakan untuk transaksi apa saja Kecuali Payment E-Commerce seperti yang sudah di Informasikan oleh Pihak Indomaret per Tanggal 1 Maret 2018 kemarin melalui akun resmi nya di Social Media. dan Tidak di perbolehkan untuk mencetak/Print Kode Voucher yang telah diterima
Untuk Tata Cara Pemakaian Kode Voucher Indomaret Digital nya adalah sebagai berikut :
· Tanyakan kepada Kasir apakah bisa pembayaran menggunakan Kode Voucher Indomaret Digital.
· Jika Bisa, Harap diperlihatkan Scan QR Code atau kode voucher
· Kasir akan menginput Kode Voucher Indomaret Digital menggunakan Scan Barcode/manual.
· Transaksi Selesai.',
                ],
                [
                    'brand_code' => 'ALFM',
                    'name' => 'Alfamart Digital',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Alfamart_Logo.png',
                    'description' => 'Voucher Alfamart Digital Code adalah Voucher Belanja non fisik yang terdiri dari 12 Digit Alpabet dan Numerik Kode Voucher yang dapat digunakan untuk transaksi yang ada di Alfamart, Alfamidi seluruh Indonesia. Untuk payment apa saja yang dapat di transaksikan di Alfamart dapat langsung ditanyakan kepada Pihak Alfamart. Tidak di perbolehkan untuk mencetak/Print Kode Voucher yang telah diterima.

Untuk Tata Cara Pemakaian Kode Voucher Alfamart Digital nya adalah sebagai berikut  :
•	Tanyakan kepada kasir apakah bisa pembayaran menggunakan Kode Voucher Digital.
•	Jika Bisa, Harap diperlihatkan Scan QR Code yang sudah ada di Menu Voucherku.
•	Kasir akan menginput Kode Voucher Alfamart Digital menggunakan Scan Barcode.
•	Transaksi Selesai.',
                ],
                [
                    'brand_code' => 'CRFR',
                    'name' => 'Carrefour Transmart',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Carrefour_Logo.png',
                    'description' => 'Carrefour hadir menjadi Sahabat berbelanja keluarga di Indonesia sejak 1998. Sejak tanggal 19 November 2012, Carrefour di Indonesia sudah dimiliki 100% sahamnya oleh CT Corp. Seiring dengan pergantian pemegang saham tersebut, nama perusahaan berubah menjadi PT. Trans Retail Indonesia dari sebelumnya PT. Carrefour Indonesia, sehingga Brand Carrefour menjadi berubah menjadi Transmart Carrefour. Hingga saat ini, terdapat 13 gerai Transmart dengan konsep one stop Shopping yang telah memiliki area khusus untuk mom& baby, Electronic pro, fashion, home living, fresh, bread shop dan juga area groceries. Dari gerai tersebut terdapat 4 gerai Transmart dengan konsep yang lebih Premium dan lifestyle dimana konsep tersebut digabung dengan area entertain yaitu arena bermain keluarga kid city-“Mini Trans Studio”.

Transmart Carrefour bermitra dengan lebih dari 4.000 pemasok dari seluruh Indonesia yang 70% dari jumlah tersebut termasuk dalam kategori Usaha Kecil dan Menengah (UKM). Melalui Transmart Carrefour, para pemasok ini dapat memberikan akses kepada pelanggan ke puluhan ribu jenis produk yang 90% nya merupakan produk lokal.

Dapat dipastikan untuk Voucher Code yang kami kirimkan valid/asli dan dalam pengawasan system.',
                ],
                [
                    'brand_code' => 'BKGM',
                    'name' => 'Bakmi GM',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Bakmi_GM.jpg',
                    'description' => 'Voucher Digital Bakmi GM adalah Voucher Belanja Non-Fisik yang dapat digunakan untuk transaksi di Seluruh Bakmi GM se-Indonesia,
Kecuali di Booth Grand City Surabaya, Store Bandara Soekarno-Hatta 2F, Booth Stasiun Bandara Soekarno-Hatta, Food  Truk Bakmi GM & Event /Bazar Bakmi GM.
Untuk tata cara Pemakaian Voucher Digital Bakmi GM nya , Jika bisa harap diperlihatkan Scan QR Code yang sudah ada di Menu Voucherku.
Kasir akan menginput Kode Voucher Digital Bakmi GM nya, menggunakan scan barcode. Dan Transaksi selesai.
Barang yang sudah dibeli tidak dapat dikembalikan, Karena  Kode Voucher yang kami kirimkan adalah Valid/Asli dan dalam pengawasan System.
',
                ],
                [
                    'brand_code' => 'ANCL',
                    'name' => 'Ancol',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/ANCOL.png',
                    'description' => 'Voucher Digital Ancol Entrance Gate.

Berlaku untuk 1 Orang 1 kali masuk Pintu Gerbang,
Maksimal Pembelian dalam 1 Account adalah 9 Voucher


Taman Impian Jaya Ancol merupakan sebuah objek wisata di bumi sari natar Jakarta Utara. Sebagai komunitas pembaharuan kehidupan masyarakat yang menjadi kebanggaan bangsa. Senantiasa menciptakan lingkungan sosial yang lebih baik melalui sajian hiburan berkualitas yang berunsur seni, budaya dan pengetahuan, dalam rangka mewujudkan komunitas \'Life Re-Creation\' yang menjadi kebanggaan bangsa.
Dapat dipastikan voucher yang kami kirim adalah Valid/Asli  dan dalam pengawasan system. ',
                ],
                [
                    'brand_code' => 'BOGA',
                    'name' => 'Boga Group',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Boga_Group_v_02.jpg',
                    'description' => 'Voucher Digital Boga Group Rp 50.000 merupakan voucher yang dapat digunakan di 10 restaurant yang bertanda Boga Group. Memudahkan anda untuk bertransaksi di Store Boga Group yang terdiri dari :  Bakerzin, Boga Catering, Pepper Lunch, Paradise Dynasty, Paradise Inn, Master Wok, Shaburi, Kintan Buffet, Onokabe, dan Putu Made.
Dengan Voucher Digital Boga Group ini anda tidak perlu lagi membawa uang cash, karna semua pembayaran ada di dalam genggaman anda. Hanya perlu beberapa klik saja hingga transaksi pembayaran selesai.
Cocok untuk Hangout bersama Keluarga, Teman atau Kerabat Terdekat.
Dapat dipastikan Voucher yang kami kirim adalah Valid/Asli dan dalam pengawasan system.',
                ],
                [
                    'brand_code' => 'BRDL',
                    'name' => 'Breadlife',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Logo_Breadlife_300x300.png',
                    'description' => 'Breadlife adalah Japanese Artisan Bakery terbesar saat ini di Indonesia, yang menghadirkan Japanese Premium Bread dengan High Quality Product yang dibuktikan dari kepuasaan para customer Breadlife selama ini. Breadlife memiliki keunggulan rasa dan tekstur yang lebih premium, tanpa bahan pengawet, dan lebih higienis karena disajikan dalam display tertutup.


Kini untuk berbelanja Product semakin mudah dengan Voucher Digital Breadlife ini.
Untuk nominal Voucher nya 50.000.


Dapat dipastikan Voucher yang kami kirim adalah Valid/Asli dan dalam pengawasan system. ',
                ],
                [
                    'brand_code' => 'DRML',
                    'name' => 'Dreamline',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Dreamline.png',
                    'description' => 'Dreamline didirikan didirikan pada tahun 1998. Seiring berjalannya waktu, kami dapat meningkatkan kualitas produk kami dan kapasitas untuk menembus pasar high-end. Kami membuka beberapa showroom di banyak pusat perbelanjaan di Jabodetabek. Kami terbukti sebagai salah satu perusahaan yang selalu berusaha untuk memperkuat citra sebagai salah satu produsen kasur berkualitas tinggi dengan beberapa perbaikan sejak tahun 1998.
Dapat dipastikan untuk Voucher Code yang kami kirimkan valid/asli dan dalam pengawasan system. ',
                ],
                [
                    'brand_code' => 'EMDG',
                    'name' => 'EmasDigi',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/EDG-Logo-300x300px-white_backg.png',
                    'description' => 'EmasDigi merupakan perusahan Financial Technology yang bergerak dalam bidang perdagangan logam mulia emas melalui aplikasi mobile. Emas yang diperdagangan berasal dari ANTAM yang terjamin kualitasnya.

EmasDigi  menawarkan cara modern bertransaksi emas, mulai dari 0,01 gram. Anda bisa dengan mudah membeli, menjual, hingga saling transfer emas antar teman secara virtual dari smartphone.

EmasDigi menyediakan program Cicilan Emas yang memfasilitasi pembelian emas dengan bunga 5% dan uang muka rendah.

Saldo emas bisa dicairkan jadi emas batangan maupun uang tunai. Dana tunai bisa ditransfer ke rekening tabungan atau ditarik di toko emas dan minimarket Alfamart.

Dengan Ppn 0.45% (apabila memiliki NPWP) dan Ppn 0,9% (tidak memiliki NPWP), bertransaksi emas membuat keuangan Anda lebih efisien. Didukung dengan sistem teknologi Aplikasi EmasDigi yang canggih, kami menjamin transaksi Anda praktis dan aman.

Harga jual beli emas sangat transparan dan terukur dalam bursa karena EmasDigi bekerja sama dengan ICDX. Yang membedakan EmasDigi dengan provider lain: kami memberikan jaminan buyback emas, berapapun saldo gram emas yang ingin dijual.


Dapat dipastikan untuk Voucher Code yang kami kirimkan valid/asli dan dalam pengawasan system.
',
                ],
                [
                    'brand_code' => 'FNCO',
                    'name' => 'Frank & Co',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/franck.jpg',
                    'description' => '1. Penggunaan Corporate Jewellery Certificate:

a. Jewellery Certificate ini dapat digunakan di seluruh store dan brands PT. CMK.
b. Jewellery Certificate berlaku sampai dengan 31 Agustus 2019.
c. Jewellery Certificate dapat digunakan tanpa ada nilai minimum pembelian.
d. Jewellery Certificate ini tidak dapat digunakan untuk pembelian Gold Bars, Gold Jewellery, berlian ukuran 0,3 carat ke atas ataupun produk bersertifikat internasional.
e. Jewellery Certificate dapat digabungkan dengan Program Promosi yang sedang berlangsung, dan Discount Card dengan rasio minimum purchase yang tertera pada Discount Card.
f. Nilai yang tertera di Jewellery Certificate tidak akan mengurangi nilai barang pada saat Trade in atau Resell. Dengan kata lain, tidak mengurangi nilai invoice pada transaksi baru, trade in atau resell.
g. Jewellery Certificate digunakan sebagai alat pembayaran pengganti uang yang diterbitkan oleh perusahan.
h. Produk yang menggunakan Jewellery Certificate ini dapat diresell dan diexchange.
i. Jewellery Certificate yang telah digunakan akan menjadi milik PT. CMK.
j. Bila ada kerusakan atau cacat pada Jewellery Certificate ini pada saat pembayaran akan dilaksanakan, PT. CMK berhak untuk memeriksa keabsahan Jewellery Certificate ini.
k. Manajemen PT. CMK akan menerima Jewellery Certificate yang asli dan ditandatangani oleh pejabat yang berwenang. l. Manajemen PT. CMK berhak mengambil keputusan terakhir atas segala perselisihan yang timbul akibat dari penggunaan Jewellery Certificate ini.

2. Penggunaan Discount Card General (Termasuk event in-store):
a. Discount Card merupakan kartu potongan yang dapat digunakan di seluruh store dan brands PT. CMK.
b. Discount Card ini berlaku sampai dengan 31 Agustus 2019.
c. Discount Card ini dapat digabungkan dengan program membership PT. CMK
d. Discount Card tidak dapat digabungkan dengan Program Promosi yang sedang berlangsung dan Birthday Voucher
e. Nilai yang tertera di Discount Card akan mengurangi nilai yang tertera di Invoice Pembelian.
f. Discount Card ini dapat digunakan untuk minimum pembelian sebesar Rp8.000.000,- ( Delapan Juta Rupiah)
g. Discount card ini hanya berlaku untuk 1(satu) invoice dengan mempertimbangkan minimum purchase yang berlaku. (Berlaku kelipatan apabila penggunaan DC lebih dari 1)
h. Discount Card yang telah digunakan akan menjadi milik PT. CMK.
i. Produk yang telah dibeli menggunakan Discount Card ini dapat diresell dan diexchange.
j. Discount Card ini tidak dapat digunakan untuk pembelian Gold Bars, Gold Jewellery, berlian ukuran 0,3 carat ke atas ataupun produk bersertifikat internasional.
k. Bila ada kerusakan atau cacat pada Discount Card ini pada saat pembayaran akan dilaksanakan, PT. CMK berhak untuk memeriksa keabsahan Discount Card ini
 l. Manajemen PT. CMK akan menerima Discount Card yang asli dan ditandatangani oleh pejabat yang berwenang.
m. Manajemen PT. CMK berhak mengambil keputusan terakhir atas segala perselisihan yang timbul akibat dari penggunaan Discount Card ini.',
                ],
                [
                    'brand_code' => 'GRMD',
                    'name' => 'Gramedia',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/Gramed_Logo_v_02.jpg',
                    'description' => 'Voucher Digital Gramedia ini hanya dapat digunakan untuk pembelian Product Buku yang ada di Gramedia.
Hadiah yang sangat cocok untuk diberikan kepada keluarga, teman, atau kerabat terdekat.
Tersedia dalam 2 nominal Voucher Code yaitu Rp. 50.000 dan Rp. 100.000
Dapat dipastikan Voucher Code yang dikirimkan adalah Valid / Asli dan dalam pengawasan system.',
                ],
                [
                    'brand_code' => 'MTRX',
                    'name' => 'Metrox',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/metrox.png',
                    'description' => 'Metrox Group adalah Group ritel gaya hidup yang didirikan pada tahun 2004 oleh dua calon visioner, Bersama-sama mereka membayangkan industri ritel terawat dengan baik yang menghadirkan hal-hal penting gaya hidup yang paling penting bagi konsumen modern. Gagasan itulah yang kemudian menjadi pilar perusahaan bisnis ritel yang berkisar dari merek fashion dan hidup internasional hingga produk aktif & wellness.

Metrox Group mewujudkan berbagai merek besar dalam portofolio yang melengkapi setiap kebutuhan pelanggan untuk bayi hingga orang dewasa. Grup ini berhasil menangani dan mengembangkan merek besar dan mewah dengan fokus yang sama sehingga menghasilkan merek terbaik untuk setiap segmentasi dengan pertumbuhan eksponensial dan telah berubah menjadi kesuksesan luar biasa. Sementara merek sendiri di Metroxgroup diakui sebagai setelan terkuat perusahaan, merek internasional yang ditangani Metroxgroup sama dengan yang dimiliki perusahaan untuk mencapai pasar yang luas dan memimpin industri.

Metrox Group Brands : Wakai, the little things she need, Keds, Rimowa, Paul Frank, Dyson, Shaga, Osim, Mezzo, Pro-Keds, Sperry.',
                ],
                [
                    'brand_code' => 'SHRT',
                    'name' => 'Share Tea',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/sharetea.png',
                    'description' => 'Didirikan pada tahun 1992, Sharetea memulai bisnisnya dengan minuman teh hitam dan teh susu mutiara di Taipei, Taiwan. Sejak itu Sharetea telah membuka banyak cabang dan waralaba di 18 negara seperti Amerika Serikat, Kanada, Inggris, Australia, Singapura, Indonesia, dan lebih dari 450 toko.

ShareTea terbuat dari daun teh berkualitas tinggi dan bahan-bahan pilihan dikirim langsung dari Taiwan. Semua produk dijamin 100% akan tiba segar dan lezat. Kami menguji daun teh setiap musim tanam dan semua bahan dari waktu ke waktu untuk memastikan minuman kami konsisten dan berkualitas. Teh terbaik membutuhkan bahan-bahan terbaik dan kami bersikeras yang terbaik untuk Anda.',
                ],
                [
                    'brand_code' => 'TAWN',
                    'name' => 'Ta Wan',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/tawan.png',
                    'description' => 'Ta Wan adalah tempat berkumpulnya kehangatan sejak  tahun 1996. Mulai dari hangatnya kekerabatan sampai hangatnya percakapan dua insan, semua bertemu bersama dengan kehangatan beragam menu Ta Wan.
Hadir dengan konsep sajian bercita rasa buatan rumah dengan rasa otentik & harga yang ramah, Ta Wan telah memberikan banyak kehangatan di lebih dari 19 kota di Indonesia. Bahkan varian bubur di Ta Wan yang nikmat memikat sekarang menjadi menu legendaris yang lekat dengan citra hangat Ta Wan. Lokasi yang mudah dijangkau & suasana yang nyaman membuat Ta Wan serasa rumah kedua. Tak heran jika Ta Wan menjadi lokasi bersantap favorit banyak keluarga di Indonesia.
Dapat dipastikan Voucher Code yang dikirimkan adalah Valid / Asli dan dalam pengawasan system..',
                ],
                [
                    'brand_code' => 'UVCR ',
                    'name' => 'Ultra Voucher',
                    'image' => 'https/ordersandbox.vouchercenter.id/assets/image/logobrand/uv.jpeg',
                    'description' => 'Ultra Voucher adalah satu voucher mobile untuk semua. Mudah dibagikan sebagai hadiah untuk keluarga, teman, atau orang lain. Ultra Voucher dapat digunakan di 100 merchant yang sudah bekerja sama. Ultra Voucher juga dapat digunakan untuk ditukar dengan Voucher Digital Alfamart, Indomaret, Boga Group, Gramedia, Bakmie GM, Breadlife,  CT Corp, Dreamline, Ta Wan Restaurant,  hingga Voucher Digital Ecommerce seperti Bukalapak dan Blibli. Atau Voucher Fisik seperti : MAP, Metrox, Carrefour, Hypermart, Superindo, Giant, IKEA, Shell, KFC, McDonalds, dan Boga Group.

Untuk Syarat dan Ketentuan Voucher Ultra Voucher / UV Balance adalah Jika nilai UV Balance yang di gunakan nominal nya lebih kecil dari yang dimiliki. Maka selisih dari nilai tersebut akan hangus.

Contoh : Jika Customer memiliki Ultra Voucher Rp.100.000 kemudian customer transaksi beli Voucher Digital Indomaret Rp. 25.000 1pcs maka selisih nilai Rp. 75.000 akan hangus atau tidak kembali.

Jika ada informasi yang kurang jelas atau mengalami kendala dapat menghubungi Contact Center kami di 08119276700 / info@ultravoucher.co.id ',
                ],
                [
                    'brand_code' => 'TSL',
                    'name' => 'Telkomsel',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/telkomsel.jpg',
                    'description' => 'Telkomsel',
                ],
                [
                    'brand_code' => 'PLN',
                    'name' => 'PLN Listrik',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/pln.png',
                    'description' => '',
                ],
                [
                    'brand_code' => 'BPJS',
                    'name' => 'BPJS',
                    'image' => 'https://ordersandbox.vouchercenter.id/assets/image/logobrand/',
                    'description' => 'For bpjs',
                ]
            ],
        ];
        //@codingStandardsIgnoreEnd
    }

    private function skusResponse(): array
    {
        return [
            'http_code' => 200,
            'status' => 'success',
            'code' => 'SSR001',
            'message' => 'Success get Sku list',
            'data' => [
                [
                    'brand_code' => 'UVCR ',
                    'sku' => 'SKU-UV-00050',
                    'sku_name' => 'Voucher Ultra Voucher Rp.  50.000',
                    'nominal' => '50000',
                    'distributor_price' => '49000',
                ],
                [
                    'brand_code' => 'UVCR ',
                    'sku' => 'VCUV100',
                    'sku_name' => 'Voucher Digital Ultra Voucher [Rp.100.000]',
                    'nominal' => '100000',
                    'distributor_price' => '98000',
                ]
            ]
        ];
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testGetPrograms()
    {
        $apiMock = \Mockery::mock(Api::class);
        $apiMock->shouldReceive('getSkus')->once()->andReturn($this->skusResponse());

        $apiMock->shouldReceive('getBrands')->once()->andReturn($this->brandsResponse());
        $this->catalogSync->setApi($apiMock);
        $programs = $this->catalogSync->getPrograms();
        static::assertArrayHasKey('SKU-UV-00050', $programs);
        static::assertCount(2, $programs);
    }

    /**
     * @throws \Http\Client\Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function testSync()
    {
        $pimple = new Container();
        $pimple[IDmo::class] = function () {
            $dmo = \Mockery::mock(\Giift\Model\IDmo::class);
            $dmo->shouldReceive('setName')->twice()->andReturn($dmo);
            $dmo->shouldReceive('setType')->twice()->andReturn($dmo);
            $dmo->shouldReceive('setImage')->andReturn($dmo);
            $dmo->shouldReceive('setUnit')->twice()->andReturn($dmo);
            $dmo->shouldReceive('getCardBuyable')->twice()->andReturn(null);
            $dmo->shouldReceive('setExternalReference')->twice()->andReturn($dmo);
            $dmo->shouldReceive('getId')->times(4)->andReturn(Uuid::uuid1()->toString());
            $dmo->shouldReceive('setStatus')->twice()->andReturn($dmo);
            $dmo->shouldReceive('setCountry')->twice()->andReturn($dmo);
            $dmo->shouldReceive('setLink')->twice()->andReturn($dmo);
            $dmo->shouldReceive('setGiiftIssued')->twice()->andReturn($dmo);
            $dmo->shouldReceive('getCategories')->twice()->andReturn([]);
            $dmo->shouldReceive('setTrackValueProvider')->twice()->withArgs(['manual'])->andReturn($dmo);
            $dmo->shouldReceive('setInfo')->twice()->withArgs(['expire', true])->andReturn($dmo);
            $dmo->shouldReceive('getName')->twice()->andReturn(Uuid::uuid1()->toString());
            $dmo->shouldReceive('getTemplateUrl')->twice()->andReturn('');

            $dmoService = \Mockery::mock(IDmo::class);
            $dmoService->shouldReceive('getFromExternalReference')->twice()->andReturn(null);
            $dmoService->shouldReceive('getNew')->twice()->andReturn($dmo);
            $dmoService->shouldReceive('save')->twice()->andReturn($dmo);

            return $dmoService;
        };
        $pimple[\Giift\Model\Service\ICategory::class] = function () {
            $category = \Mockery::mock(ICategory::class);
            $service = \Mockery::mock(\Giift\Model\Service\ICategory::class);
            $service->shouldReceive('getOne')->once()->andReturn($category);
            return $service;
        };
        $pimple[\Giift\Model\Service\IDmoCategory::class] = function () {
            $category = \Mockery::mock(IDmoCategory::class);
            $category->shouldReceive('setDmo')->twice()->andReturn($category);
            $category->shouldReceive('setCategory')->twice()->andReturn($category);

            $service = \Mockery::mock(\Giift\Model\Service\IDmoCategory::class);
            $service->shouldReceive('getNew')->twice()->andReturn($category);
            $service->shouldReceive('save')->twice()->andReturn($category);
            return $service;
        };

        $pimple[\Giift\Model\Service\ITag::class] = function () {
            $tag = \Mockery::mock(ITag::class);
            $tag->shouldReceive('setDmo')->twice()->andReturn($tag);
            $tag->shouldReceive('setName')->twice()->andReturn($tag);
            $tag->shouldReceive('setTag')->twice()->andReturn($tag);
            $tag->shouldReceive('setDisplayName')->twice()->andReturn($tag);

            $tagsService = \Mockery::mock(\Giift\Model\Service\ITag::class);
            $tagsService->shouldReceive('getList')->twice()->andReturn([]);
            $tagsService->shouldReceive('save')->twice()->withArgs([$tag])->andReturn($tag);
            $tagsService->shouldReceive('getNew')->twice()->andReturn($tag);
            return $tagsService;
        };

        $pimple[IBuyable::class] = function () {
            $buyable = \Mockery::mock(\Giift\Model\IBuyable::class);
            $buyable->shouldReceive('setTnc')->twice()->andReturn($buyable);
            $buyable->shouldReceive('setActive')->twice()->withArgs([false])->andReturn($buyable);
            $buyable->shouldReceive('setTypeId')->twice()->andReturn($buyable);
            $buyable->shouldReceive('setType')->twice()->andReturn($buyable);
            $buyable->shouldReceive('setFactoryName')->twice()->andReturn($buyable);
            $buyable->shouldReceive('setInfo')->twice()->andReturn($buyable);

            $buyableService = \Mockery::mock(IBuyable::class);
            $buyableService->shouldReceive('save')->twice()->andReturn($buyable);
            $buyableService->shouldReceive('getNew')->twice()->andReturn($buyable);
            return $buyableService;
        };

        $apiMock = \Mockery::mock(Api::class);
        $apiMock->shouldReceive('getSkus')->once()->andReturn($this->skusResponse());

        $apiMock->shouldReceive('getBrands')->once()->andReturn($this->brandsResponse());
        $this->catalogSync
            ->setApi($apiMock)
            ->setContainer(new \Pimple\Psr11\Container($pimple));
        $this->catalogSync->syncAll();
        static::assertTrue(true);
        \Mockery::close();
    }
}
